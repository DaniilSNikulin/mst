#include "../tree.hpp"
#include <iostream>
#include <ctime>

int main(int argc, char *argv[])
{
	graph extrn;
	std::string filename ("../tables/in_5.dat");
	if (argc > 1)
		filename = argv[1];
	extrn = gen_graph (filename);
//	int rude = extrn.rude_min_weight;
	clock_t start = clock();
	ptr_edges mst = Prim (extrn);
	clock_t end = clock();
	print_tree (mst);
//	std::cout << "rude: " << rude << '\n';
	std::cout << "diff_time: " << end - start << '\n';
}

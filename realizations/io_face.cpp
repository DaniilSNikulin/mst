#include "../tree.hpp"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cstdlib>


void print_tree ( const ptr_edges& mst )
{
	int summ_weight {0};
	for (auto mst_edge : *mst)
	{
		summ_weight += mst_edge.get_wght();
		std::cout << "{ " << std::setw(2) << mst_edge.get_fnode().get_id() << " :"
		          << std::setw(2) << mst_edge.get_wght() << ": "
		          << std::setw(2) << mst_edge.get_snode().get_id() << " }\n";
	}
	std::cout << "size: " << mst->size() << '\n';
	std::cout << "summ weight: " << summ_weight << '\n';
}


graph gen_graph (const std::string& filename)
{
	std::ifstream inp ( filename.c_str() );
	if ( !inp ) {
		std::string mess {"error in open: " + filename };
		throw std::runtime_error (mess.c_str());
	}
	int dim;
	inp >> dim;
	if (dim < 1) {
		throw std::runtime_error ("error in dim: negativ or zero");
	}

	graph storage;
	for (int i = 0; i < dim; ++i)
	{
		node new_node {i};
		storage[new_node] = std::make_shared<std::multiset<edge> > ();
	}
	for (auto left_pair : storage)
	{
		for (auto right_pair : storage)
		{
			double k;
			inp >> k;
			if (k > 0) {
				edge tmp {left_pair.first, right_pair.first, k};
				left_pair.second->insert (tmp);
			}
		}
	}
	inp >> storage.rude_min_weight;
	return storage;
}

#include <iostream>
#include <algorithm>
#include <cstddef>
#include <thread>
#include "../tree.hpp"

namespace
{
	struct set_key
	{
		graph in_tree;
		std::pair<node, edge> min_pair;

		set_key () = delete;
		set_key (const graph& include)
			: in_tree {include}, min_pair {}
		{}

		inline node& frst() { return min_pair.first; }
		inline edge& scnd() { return min_pair.second; }

		void operator () (std::pair<node, ptr_edges> ex_pair)
		{
			edge min_edge { scnd() };
			for (auto in_pair : in_tree)
			{
				ptr_edges ex_edges {ex_pair.second};
				auto local_min_it = std::find_if (ex_edges->begin(), ex_edges->end(),
						[&in_pair] (edge elem) {return elem.is_near (in_pair.first);} );

				if (local_min_it != ex_edges->end()) {
					min_edge = std::min (min_edge, *local_min_it);
				}
			}

			if (min_edge < scnd()) {
				frst() = ex_pair.first;
				scnd() = min_edge;
			}
		}
	};
} // namespace

ptr_edges Prim ( graph& extrn )
{
	ptr_edges que {std::make_shared< std::multiset<edge> >()};
	// if ( extrn.empty() ) {
	// 	return que;
	// }

	graph include;
	std::pair<node, edge> Default;
	include.insert ( *extrn.begin() );
	extrn.erase ( extrn.begin() );

	while ( extrn.size() )
	{
		set_key setter {include};
		setter = std::for_each (extrn.begin(), extrn.end(), setter);
		if (setter.min_pair == Default)   // exit if the graph is not connected or empty
			break;
		que->insert ( setter.scnd() );
		include.insert ( *extrn.find(setter.frst()) );
		extrn.erase ( setter.frst() );
	}
return que;
}

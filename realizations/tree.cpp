#include "../tree.hpp"
#include <fstream>
#include <cmath>


bool operator < (const node& first, const node& second)
{
	return first.get_id() < second.get_id();
}
bool operator == (const node& first, const node& second)
{
	return first.get_id() == second.get_id();
}


bool operator < (const edge& first, const edge& second)
{
	return first.get_wght() < second.get_wght();
}
bool operator == (const edge& first, const edge& second)
{
	bool cmp_wght  {first.get_wght() == second.get_wght()};
	bool cmp_node1 {first.get_fnode() == second.get_fnode()};
	bool cmp_node2 {first.get_fnode() == second.get_snode()};
	cmp_node1 &= first.get_snode() == second.get_snode();
	cmp_node2 &= first.get_snode() == second.get_fnode();
	return cmp_wght && (cmp_node1 || cmp_node2);
}


edge::edge (node first, node second, double weight)
	:  my_weight {weight}, my_nodes {first, second}
{}

edge::edge()
	: my_weight {INFINITY}, my_nodes {-1, -1}
{}

bool edge::is_near (node first, node second) const
{
	bool ret {first==my_nodes.first && second==my_nodes.second};
	ret = ret || (first==my_nodes.second && second==my_nodes.first);
	return ret;
}
bool edge::is_near (node other) const
{
	return other==my_nodes.first || other==my_nodes.second;
}
